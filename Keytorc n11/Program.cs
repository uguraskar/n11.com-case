﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;

namespace Keytorc_N11_Console {
    class Program {
        static void Main(string[] args) {
            IWebDriver driver = new FirefoxDriver();
            driver.Url="https://www.n11.com/";

            Actions action = new Actions(driver);

            LoginPage login = new LoginPage(driver);
            login.typeUserName();
            login.typePassword();
            login.clickOnLoginButton();

            SearchItem search = new SearchItem(driver);
            search.typeSearchText();
            search.clickOnSearchButton();
            search.isExist();
            search.gotoSecondPage();
            search.confirmSecondPage();

            Favourites favs = new Favourites(driver, action);
            favs.addThirdToFavs();
            favs.hoverOnMyAccountHolder();
            favs.gotoFavs();
            favs.selectFirstFav();
            favs.isProductSame();
            favs.deleteFromFavs();
            favs.isProductDeletedFromFavs();

            //driver.Quit();
            }
        }
    }
